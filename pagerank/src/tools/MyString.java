package tools;

import java.util.ArrayList;

public class MyString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * 根据一行String，类型为A B C 转为整数数组
	 * 
	 * @param oneLine
	 * @return
	 */
	public static ArrayList<Integer> lineToIntegers(String oneLine) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		if (oneLine.length() == 0) {
			return list;
		}
		String[] tt = oneLine.split(" ");

		for (String t : tt) {
			list.add(Integer.valueOf(t));
		}
		return list;
	}

}
