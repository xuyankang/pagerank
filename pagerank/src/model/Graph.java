package model;

import java.io.File;

public interface Graph {
	public int V();

	public void createFromFile(String filePath);
	
	public int E();

	public void addEdge(int v, int w);

	public Iterable<Integer> adj(int v);

	public String toString();
}
