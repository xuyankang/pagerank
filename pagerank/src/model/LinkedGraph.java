package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.tools.Tool;

import tools.MyString;

public class LinkedGraph {

	private int V;
	private int E;
	private ArrayList<ArrayList<Integer>> graph;

	public void createLinkedGraph(int v) {
		construct(v);
		for (int i = 0; i < V; i++) {
			ArrayList<Integer> oneList = new ArrayList<Integer>();
			graph.add(oneList);
		}
	}

	private void construct(int v) {
		this.V = v;
		this.E = 0;
		graph = new ArrayList<ArrayList<Integer>>();
	}

	public LinkedGraph(String filePath) {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(
				filePath)))) {
			int v = Integer.valueOf(br.readLine());
			construct(v);
			for (int i = 0; i < v; i++) {
				graph.add(MyString.lineToIntegers(br.readLine().trim()));
			}
		} catch (IOException e) {
			System.out.println(e);
		}

	}

	public String toString() {
		return graph.toString();
	}

	/**
	 * 节点编号从0开始
	 * 
	 * @param vertex
	 * @return
	 */
	public ArrayList<Integer> getInArray(int vertex) {
		// 遍历所有结点，找到所有出度中含有 vertex 的节点
		ArrayList<Integer> inArray = new ArrayList<Integer>();
		for (int i = 0; i < graph.size(); i++) {
			ArrayList<Integer> list = graph.get(i);
			for (int j = 0; j < list.size(); j++) {
				if (vertex == list.get(j)) {
					inArray.add(i);
					break;
				}
			}
		}
		return inArray;
	}

	/**
	 * the vertex start from 0
	 * 
	 * @param vertex
	 * @return
	 */
	public ArrayList<Integer> getOutArray(int vertex) {
		return graph.get(vertex);
	}

	public int getV() {
		return V;
	}

	public int size() {
		return V;
	}
}
