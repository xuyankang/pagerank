package test;

import algorithm.PageRank;
import model.LinkedGraph;

public class Test {
	public static void main(String[] args) {
		// read relative path
		String filePath = "src/graph.txt";
		LinkedGraph graph = new LinkedGraph(filePath);
		System.out.println(graph);
		System.out.println(graph.getInArray(0));
		System.out.println(graph.getOutArray(0));

		PageRank pr = new PageRank(graph);
		for (int i = 0; i <50; i++) {
			pr.calculate();
		}
		System.out.println(pr);
	}
}
