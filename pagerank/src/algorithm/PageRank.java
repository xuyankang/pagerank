package algorithm;

import java.util.ArrayList;

import model.LinkedGraph;

public class PageRank {

	private ArrayList<Double> currentScore;
	private ArrayList<Double> nextScore;
	private ArrayList<ArrayList<Double>> historyScore;
	private LinkedGraph graph;
	private double dampingFactor = 0.85;
	private boolean isConvergence = false;
	private double convergenceThreshold = 0.001;

	public PageRank(LinkedGraph graph) {
		currentScore = new ArrayList<Double>();
		double initScore = 1.0 / graph.getV();
		for (int i = 0; i < graph.getV(); i++) {
			currentScore.add(0.250);
		}
		init(currentScore, graph);
	}

	public void calculate() {
		for (int i = 0; i < graph.getV(); i++) {
			double sum = 0;
			// sum in degree
			for (Integer ii : graph.getInArray(i)) {
				sum += currentScore.get(ii) / graph.getOutArray(ii).size();
			}
			sum = (1 - dampingFactor) / graph.getV() + dampingFactor * sum;
			nextScore.set(i, sum);
		}

		boolean isConvergenceTemp = true;
		for (int i = 0; i < graph.getV(); i++) {
			if (Math.abs(currentScore.get(i) - nextScore.get(i)) > convergenceThreshold) {
				isConvergenceTemp = false;
			}
			// if one score greater than threshold,then break
			if (!isConvergenceTemp) {
				break;
			}
		}
		if (isConvergenceTemp) {
			isConvergence = true;
		} else {
			historyScore.add((ArrayList<Double>) nextScore.clone());
			currentScore = (ArrayList<Double>) nextScore.clone();
		}
	}

	public PageRank(ArrayList<Double> currentScore, LinkedGraph graph) {
		init(currentScore, graph);
	}

	private void init(ArrayList<Double> currentScore, LinkedGraph graph) {
		this.graph = graph;
		this.currentScore = currentScore;
		nextScore = new ArrayList<Double>();
		for (int i = 0; i < graph.getV(); i++) {
			nextScore.add(0.0);
		}
		historyScore = new ArrayList<ArrayList<Double>>();
		historyScore.add((ArrayList<Double>) currentScore.clone());
	}

	public String toString() {
		String result = "";
		int count = 1;
		for (ArrayList<Double> ds : historyScore) {
			result += String.format("%02d :\t", count++);
			for (Double d : ds) {
				result += String.format("%.3f\t", d);
			}
			result += "\n";
		}
		result+="pagerank has reached convergence.\n";
		return result;
	}
}
